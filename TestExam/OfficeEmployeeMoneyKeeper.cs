﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestExam
{
    class OfficeEmployeeMoneyKeeper : OfficeEmp
    {
        double salaryPerMonth; //ставка в месяц
        internal bool free;

        public double SalaryPerMounth
        {
            get
            {
                if (salaryPerMonth <= 0)
                    return 0;
                return salaryPerMonth;
            }
            set
            {
                salaryPerMonth = value;
            }
        }
        public OfficeEmployeeMoneyKeeper() : base()
        {
            Name = "Бухгалтер";
        }

        public OfficeEmployeeMoneyKeeper(double hour, double salaryPM)
        {
            Name = "Бухгалтер";
            hour = 1;
            this.salaryPerMonth = SalaryPerMounth; //месячная ставка
            free = true;
        }

        public override void StartJob()
        {
            free = false;
            //метод начисления зп за выполненный объем работы
            GetPay();
            Console.WriteLine("За выполнение этой задачи он получил {0} баксов", Sum.ToString());
        }
        public async void GetPay()
        {
            Sum += this.HourlyRate * this.TimeWorked;
            await Task.Delay(1000);
            free = true;
        }
    }
}
