﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestExam
{
    sealed class OfficeEmployeeBoss:OfficeEmp
    {
        double salaryPerMonth; //ставка в месяц

        public double SalaryPerMounth {
            get {
                if (salaryPerMonth <= 0)
                    return 0;
                return salaryPerMonth;
            }
            set {
                salaryPerMonth = value;
            }
        }
        public OfficeEmployeeBoss()
        {
            Name = "Директор";
        }
        public OfficeEmployeeBoss(double timeWorked, double salaryPM)
        {
            Name = "Директор";
            salaryPM = SalaryPerMounth; //ставка в неделю ставка
            Sum = 0;
        }
    }
}
