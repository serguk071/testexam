﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace TestExam
{
    class MounthWork
    {

        //переменные для задания количества рабочих часов в месяце
        double hoursInMounth = 161;//количество частов в месяце
        double workMounth = 1; //с какого часа начинать работу в новом месяце
        //----------------------------------------------------------
        
        //Список выполняемых заданий
        List<string> jobOffer = new List<string> { "Писать код", "Рисовать макет", "Тестировать программу", "Продавать услуги", "Cделать отчет"};
        //--------------------------

        //задаем количество работников в компании
        int startOfficeEmpCount = 10;
        int endOfficeEmpCount = 12;
        //---------------------------------------

        //недельные заработные платы сотрудников с фиксированной оплатой труда
        double bossRatePerWeek = 40;
        double moneyKeeperRatePerWeek = 30;
        double managerRatePerWeek = 25;
        //--------------------------------------------------------------------

        public void Working()
        {
            //ставка для фрилансера
            Random rndFreekanceEmpRate = new Random((int)DateTime.Now.Ticks);
            Thread.Sleep(15);
            double rateForFreelanceWorker = rndFreekanceEmpRate.Next(10, 30);
            Random rndFreelanceEmpTime = new Random((int)DateTime.Now.Ticks);
            Thread.Sleep(9);
            double timeFreelanceWorking = rndFreelanceEmpTime.Next(1, 3);
            //---------------------

            //рандом для создания сотрудников в пределах от 10 до 100
            Random rand = new Random();
            int workerCount = rand.Next(startOfficeEmpCount, endOfficeEmpCount);
            //-------------------------------------------------------

            //создали списсок работников
            List<OfficeEmp> workers = new List<OfficeEmp>();
            workers.Add(new OfficeEmployeeBoss(1, bossRatePerWeek)); //мемячный оклад директора
            workers.Add(new OfficeEmployeeMoneyKeeper(1, moneyKeeperRatePerWeek));
            workers.Add(new OfficeEmployeeManager(managerRatePerWeek));
            for (int i = 3; i <= workerCount; i++)
            {
                //ставка для офисного сотрудника
                Random rndRateForOfficeEmp = new Random((int)DateTime.Now.Ticks);
                Thread.Sleep(11);
                double rateForOfficeEmployee = rndRateForOfficeEmp.Next(1, 21);
                Random rndTimeForOfficeEmp = new Random((int)DateTime.Now.Ticks);
                Thread.Sleep(6);
                double timeOfficeEmployeeWorking = rndTimeForOfficeEmp.Next(1, 6);
                //------------------------------


                Random rnd2 = new Random((int)DateTime.Now.Ticks);
                int workerListrandom = rnd2.Next(1, 9);
                Thread.Sleep(20);
                if (workerListrandom == 1)
                {
                    workers.Add(new OfficeEmp(timeOfficeEmployeeWorking, rateForOfficeEmployee, true, true, true));
                }
                else if (workerListrandom == 2)
                {
                    workers.Add(new OfficeEmp(timeOfficeEmployeeWorking, rateForOfficeEmployee, true, true, false));
                }
                else if (workerListrandom == 3)
                {
                    workers.Add(new OfficeEmp(timeOfficeEmployeeWorking, rateForOfficeEmployee, true, false, true));
                }
                else if (workerListrandom == 4)
                {
                    workers.Add(new OfficeEmp(timeOfficeEmployeeWorking, rateForOfficeEmployee, false, true, true));
                }
                else if (workerListrandom == 5)
                {
                    workers.Add(new OfficeEmp(timeOfficeEmployeeWorking, rateForOfficeEmployee, false, true, false));
                }
                else if (workerListrandom == 6)
                {
                    workers.Add(new OfficeEmp(timeOfficeEmployeeWorking, rateForOfficeEmployee, false, false, false));
                }
                else if (workerListrandom == 7)
                {
                    workers.Add(new OfficeEmp(timeOfficeEmployeeWorking, rateForOfficeEmployee, true, false, false));
                }
                else if (workerListrandom == 8)
                {
                    workers.Add(new OfficeEmployeeMoneyKeeper(1, moneyKeeperRatePerWeek)); //бухгалтер
                }
                else
                {
                    workers.Add(new OfficeEmp(timeOfficeEmployeeWorking, timeOfficeEmployeeWorking));
                }
            }
            FreelanseEmp fr = new FreelanseEmp(timeFreelanceWorking, rateForFreelanceWorker);
            //------------------------------------------------------------------------------------------------------------

            //Выводим полученные данные на экран
            Console.WriteLine(workers.Count);
            foreach (OfficeEmp oe in workers)
            {
                oe.JobAction();
            }
            Console.WriteLine();
            //----------------------------------

            //список задач от директора
            List<string> jobList = new List<string>();
            //-------------------------

            //счетчик который проходит по рабочим часам в установленном месяце
            while (workMounth < hoursInMounth) //счетчик который проходит по рабочим часам в установленном месяце
            {
                //лист задач
Console.WriteLine("\tПрошло {0} часов", workMounth);
                
                //случайная выдача заданий
                Random rnd = new Random((int)DateTime.Now.Ticks);
                Thread.Sleep(7);
                int countTask = rnd.Next(0, 4);
                //------------------------

                //ищим в цикле исполнителя
                for (int num = 0; num <= countTask; num++) //сколько задач выдаст директор
                {
                    Random rn = new Random((int)DateTime.Now.Ticks);
                    Thread.Sleep(14);
                    int count = rn.Next(0, countTask);

                    Console.WriteLine(workers[0].Name); //должность сотрудника выдающего команды
                    jobList.Add(jobOffer[count].ToString());//команда, которая будет помещена в коллекцию комманд от директора
                    Console.WriteLine("   Задание {0}", jobOffer[count].ToString());

                    //поиск исполнителя
                    if (jobOffer[count].ToString() == "Писать код")
                    {
                        for (int i = 0; i < workers.Count; i++)
                        {
                            if (workers[i].Name.ToString() == "Програмист" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                            else if (workers[i].Name.ToString() == "Програмист-тестировщик" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                            else if (workers[i].Name.ToString() == "Програмист-дизайнер" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                            else if (workers[i].Name.ToString() == "Програмист-дизайнер-тестировщик" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                            else if (workers[i].Name.ToString() == "Програмист" && workers[i].free == false)
                            {
                                for (int k = 0; k < workers.Count; k++)
                                {
                                    if (workers[k].Name.ToString() == "Програмист" && workers[k].free == true)
                                    {
                                        workers[k].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[k].Name);
                                        break;
                                    }
                                    else if (workers[k].Name.ToString() == "Програмист-тестировщик" && workers[k].free == true)
                                    {
                                        workers[k].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[k].Name);
                                        break;
                                    }
                                    else if (workers[k].Name.ToString() == "Програмист-дизайнер" && workers[k].free == true)
                                    {
                                        workers[i].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[k].Name);
                                        break;
                                    }
                                    else if (workers[k].Name.ToString() == "Програмист-дизайнер-тестировщик" && workers[k].free == true)
                                    {
                                        workers[k].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[k].Name);
                                        break;
                                    }
                                    else if (workers[k].Name.ToString() == "Дизайнер" || workers[k].Name.ToString() == "Тестировщик-дизайнер" || workers[k].Name.ToString() == "Тестировщик" && workers[k].free == true)
                                    {
                                        Console.WriteLine("Работа ушла на фриланс");
                                        fr.GetPay();
                                        Console.WriteLine("\t\t Задание выполняет {0} за {1} баксов", fr.Name, fr.HourlyRate * fr.TimeWorked);
                                        break;
                                    }
                                }
                            }
                            else if (workers[i].Name.ToString() == "Програмист-тестировщик" && workers[i].free == false)
                            {
                                for (int s = 0; s < workers.Count; s++)
                                {
                                    if (workers[s].Name.ToString() == "Програмист" && workers[s].free == true)
                                    {
                                        workers[s].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[s].Name);
                                        break;
                                    }
                                    else if (workers[s].Name.ToString() == "Програмист-тестировщик" && workers[s].free == true)
                                    {
                                        workers[s].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[s].Name);
                                        break;
                                    }
                                    else if (workers[s].Name.ToString() == "Програмист-дизайнер" && workers[s].free == true)
                                    {
                                        workers[s].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[s].Name);
                                        break;
                                    }
                                    else if (workers[s].Name.ToString() == "Програмист-дизайнер-тестировщик" && workers[s].free == true)
                                    {
                                        workers[s].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[s].Name);
                                        break;
                                    }
                                    break;
                                }
                            }
                            else if (workers[i].Name.ToString() == "Програмист-дизайнер" && workers[i].free == false)
                            {
                                for (int j = 0; j < workers.Count; j++)
                                {
                                    if (workers[j].Name.ToString() == "Програмист" && workers[j].free == true)
                                    {
                                        workers[j].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[j].Name);
                                        break;
                                    }
                                    else if (workers[j].Name.ToString() == "Програмист-тестировщик" && workers[j].free == true)
                                    {
                                        workers[j].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[j].Name);
                                        break;
                                    }
                                    else if (workers[j].Name.ToString() == "Програмист-дизайнер" && workers[j].free == true)
                                    {
                                        workers[j].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[j].Name);
                                        break;
                                    }
                                    else if (workers[j].Name.ToString() == "Програмист-дизайнер-тестировщик" && workers[j].free == true)
                                    {
                                        workers[j].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[j].Name);
                                        break;
                                    }
                                    break;
                                }
                            }
                            else if (workers[i].Name.ToString() == "Програмист-дизайнер-тестировщик" && workers[i].free == false)
                            {
                                for (int x = 0; x < workers.Count; x++)
                                {
                                    if (workers[x].Name.ToString() == "Програмист" && workers[x].free == true)
                                    {
                                        workers[x].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[x].Name);
                                        break;
                                    }
                                    else if (workers[x].Name.ToString() == "Програмист-тестировщик" && workers[x].free == true)
                                    {
                                        workers[x].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[x].Name);
                                        break;
                                    }
                                    else if (workers[x].Name.ToString() == "Програмист-дизайнер" && workers[x].free == true)
                                    {
                                        workers[x].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[x].Name);
                                        break;
                                    }
                                    else if (workers[x].Name.ToString() == "Програмист-дизайнер-тестировщик" && workers[x].free == true)
                                    {
                                        workers[x].StartJob();
                                        Console.WriteLine("\t\t Задание выполняет {0}", workers[x].Name);
                                        break;
                                    }
                                    break;
                                }
                            }
                            //else
                            //{
                            //    Console.WriteLine("Работа ушла на фриланс");
                            //    fr.GetPay();
                            //    Console.WriteLine("\t\t Задание выполняет {0} за {1} баксов", fr.Name, fr.HourlyRate * fr.TimeWorked);
                            //    break;
                            //}
                        }
                    }
                    else if (jobOffer[count].ToString() == "Рисовать макет")
                    {
                        for (int i = 0; i < workers.Count; i++)
                        {
                            if (workers[i].Name.ToString() == "Дизайнер" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                            else if (workers[i].Name.ToString() == "Програмист-дизайнер" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                            else if (workers[i].Name.ToString() == "Тестировщик-дизайнер" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                            else if (workers[i].Name.ToString() == "Програмист-дизайнер-тестировщик" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                            else if (workers[i].Name.ToString() == "Дизайнер" || workers[i].Name.ToString() == "Програмист-дизайнер" || workers[i].Name.ToString() == "Тестировщик-дизайнер" || workers[i].Name.ToString() == "Програмист-дизайнер-тестировщик" && workers[i].free == false)
                            {
                                Console.WriteLine("Работа ушла на фриланс");
                                fr.GetPay();
                                Console.WriteLine("\t\t Задание выполняет {0} за {1} баксов", fr.Name, fr.HourlyRate * fr.TimeWorked);
                                break;
                            }
                        }
                    }
                    else if (jobOffer[count].ToString() == "Тестировать программу")
                    {
                        for (int i = 0; i < workers.Count; i++)
                        {
                            if (workers[i].Name.ToString() == "Тестировщик" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                            else if (workers[i].Name.ToString() == "Тестировщик-дизайнер" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                            else if (workers[i].Name.ToString() == "Програмист-тестировщик" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                            else if (workers[i].Name.ToString() == "Програмист-дизайнер-тестировщик" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                            else if (workers[i].Name.ToString() == "Тестировщик" || workers[i].Name.ToString() == "Тестировщик-дизайнер" || workers[i].Name.ToString() == "Програмист-тестировщик" || workers[i].Name.ToString() == "Програмист-дизайнер-тестировщик" && workers[i].free == false)
                            {
                                Console.WriteLine("Работа ушла на фриланс");
                                fr.GetPay();
                                Console.WriteLine("\t\t Задание выполняет {0} за {1} баксов", fr.Name, fr.HourlyRate * fr.TimeWorked);
                                break;
                            }
                        }
                    }
                    else if (jobOffer[count].ToString() == "Cделать отчет")
                    {
                        for (int i = 0; i < workers.Count; i++)
                        {
                            if (workers[i].Name.ToString() == "Бухгалтер" && workers[i].free == true)
                            {
                                workers[i].StartJob();
                                Console.WriteLine("\t\t Задание выполняет {0}", workers[i].Name);
                                break;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Работа ушла на фриланс");
                        fr.GetPay();
                        Console.WriteLine("\t\t Задание выполняет {0} за {1} баксов", fr.Name, fr.HourlyRate * fr.TimeWorked);
                    }
                }
                ++workMounth;

                if (workMounth == 40 || workMounth == 80 || workMounth == 120 || workMounth == 160)
                {
                    workers[1].GetPay();
                    for (int i = 1; i < workers.Count; i++)
                    {
                        Console.Write(workers[i].Name.ToString() + " ");
                        Console.WriteLine(workers[i].Sum.ToString());
                    }

                }
            }


            BinaryFormatter binFormat = new BinaryFormatter();
            Console.WriteLine("\\---------------------\\");
            Console.WriteLine("ИТОГИ РАБОТЫ ЗА МЕСЯЦ");

            Console.Write(workers[0].Name.ToString()+" ");
            Console.WriteLine(workers[0].HourlyRate.ToString());
            Console.Write(workers[1].Name.ToString() + " ");
            Console.WriteLine(workers[1].HourlyRate.ToString());
            Console.Write(workers[2].Name.ToString() + " ");
            Console.WriteLine(workers[2].HourlyRate.ToString());

            for (int i = 3; i < workers.Count; i++)
            {
                Console.Write(workers[i].Name.ToString()+" ");
                Console.WriteLine(workers[i].Sum.ToString());
            }
            Console.Write(fr.Name.ToString());
            Console.WriteLine(fr.Sum.ToString());

            try
            {
                using (StreamWriter sw = new StreamWriter("OneMounthWork.txt"))
                {
                    foreach (OfficeEmp oe in workers)
                    {
                        sw.Write(oe.Name);
                        sw.Write("\t\t\t");
                        sw.Write(oe.Sum);
                        sw.Write("\r");
                    }
                    sw.Write(fr.Name);
                    sw.Write("\t\t\t");
                    sw.Write(fr.Sum);
                    sw.Write("\r");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Записать не удалось, ошибка {0}", ex);
            }
        }
    }
}   