﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestExam
{
    class OfficeEmployeeManager:OfficeEmp
    {
        double salaryPerMonth; //ставка в месяц

        public double SalaryPerMounth
        {
            get
            {
                if (salaryPerMonth <= 0)
                    return 0;
                return salaryPerMonth;
            }
            set
            {
                salaryPerMonth = value;
            }
        }
        public OfficeEmployeeManager()
        {
            Name = "Менеджер";
        }

        public OfficeEmployeeManager(double salaryPM)
        {
            Name = "Менеджер";
            salaryPM = SalaryPerMounth; //месяная ставка
        }
    }
}
