﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestExam
{
    sealed class FreelanseEmp:Emp
    {
        public double Sum { get; set; }
        public string Name { get; set; }

        public FreelanseEmp(double timeWorked, double hourlyRate) : base(timeWorked, hourlyRate)
        {
            Name = "Фрилансер";
            Sum = 0;
        }

        public void GetPay()
        {
            Random rndFrelance = new Random((int)DateTime.Now.Ticks);
            double pay = rndFrelance.NextDouble();

            Sum += 40;
        }
    }
}
