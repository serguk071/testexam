﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestExam
{
    abstract class Emp
    {
        double timeWorked;
        double hourlyRate;

        public double TimeWorked
        {
            get
            {
                if (timeWorked < 0)
                    return 0;
                return timeWorked;
            }
            set
            {
                timeWorked = value;
            }
        }

        public double HourlyRate
        {
            get
            {
                if (hourlyRate > 40)
                    return 40;
                return hourlyRate;
            }
            set
            {
                hourlyRate = value;
            }
        }

        public string NameEmployee { get; protected set; }

        public Emp()
        {
            timeWorked = 0;
            hourlyRate = 0;
            NameEmployee = "no name";
        }

        public Emp(double timeWorked, double hourlyRate)
        {
            this.timeWorked = timeWorked;
            this.hourlyRate = hourlyRate;
            NameEmployee = "работник";

        }

        public virtual void StartJob()
        {
            Console.WriteLine("За выполнение этой задачи есть стоимость в баксах");
        }
    }
}
