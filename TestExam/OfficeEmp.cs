﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestExam
{
    internal class OfficeEmp:Emp,IJobAction
    {
        int tymeToSleepOfficeEmployee = 800; //время на выполнение задачи сотружником

        public string Name { get; set; }
        internal bool free;
        public double Sum { get; set; }
        public OfficeEmp():base()
        {
        }

        public OfficeEmp(double timeWorked, double hourlyRate) :base(timeWorked, hourlyRate)
        {
            Name = "неопределенный сотрудник";
        }

        public OfficeEmp(double timeWorked, double hourlyRate, bool programer, bool designer, bool tester ) : base(timeWorked, hourlyRate)
        {
            if (programer == true & designer == true & tester == true)
            {
                Name="Програмист-дизайнер-тестировщик";
                free=true;
                Sum = 0;
            }
            else if (programer == true & designer == true & tester == false)
            {
                Name="Програмист-дизайнер";
                free = true;
                Sum = 0;
            }
            else if (programer == true & designer == false & tester == true)
            {
                Name="Програмист-тестировщик";
                free = true;
                Sum = 0;
            }
            else if (programer == false & designer == true & tester == true)
            {
                Name="Тестировщик-дизайнер";
                free = true;
                Sum = 0;
            }
            else if (programer == true & designer == false & tester == false)
            {
                Name="Програмист";
                free = true;
                Sum = 0;
            }
            else if (programer == false & designer == true & tester == false)
            {
                Name="Дизайнер";
                free = true;
                Sum = 0;
            }
            else
            {
                Name="Тестировщик";
                free = true;
                Sum = 0;
            }
        }

        //public OfficeEmp(double hourlyRate, bool manager, bool moneyKeeper) : base()
        //{
        //    if (manager == true & moneyKeeper == false)
        //    {
        //        Name = "Менеджер";
        //        free = true;
        //        Sum = 0;
        //    }
        //    else
        //    {
        //        Name = "Бухгалтер";
        //        free = true;
        //        Sum = 0;
        //    }
        //}

        //должность сотрудника
        public void JobAction()
        {
            Console.WriteLine(Name);
        }

        public override void StartJob()
        {
            free = false;
            //метод начисления зп за выполненный объем работы
            GetPay();
            Console.WriteLine("За выполнение этой задачи он получил {0} баксов", this.HourlyRate*this.TimeWorked);
        }
        public async void GetPay()
        {
            Sum += this.HourlyRate * this.TimeWorked;
            await Task.Delay(tymeToSleepOfficeEmployee);
            free = true;
        }

        //выдача работы
        public string GetJob(List<string> str)
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            int res = rnd.Next(0, 4);
            Thread.Sleep(5);
            string result = str[res].ToString();
            return result;
        }
    }
}
